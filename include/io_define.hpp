#ifndef _IO_DEFINE_HPP
#define _IO_DEFINE_HPP


/*       
                +-----------------------+                       
           Dbg -|  1 D1/Tx       VIN  1 |- PWR                      
           Dbg -|  2 D0/Rx       GND  2 |- PWR                  
              X-|  3 RESET     RESET  3 |-X                      
           PWR -|  4 GND          5V  4 |- PWR                  
          Temp -|  5 D2           A7  5 |-                      
       Relay 1 -|  6 D3           A6  6 |-                      
       Relay 2 -|  7 D4           A5  7 |- OLED DISP. SCL                  
       Relay 3 -|  8 D5           A4  8 |- OLED DISP. SDA                  
       Relay 4 -|  9 D6           A3  9 |-                      
       Relay 5 -| 10 D7           A2 10 |- Encoder Btn                  
       Relay 6 -| 11 D8           A1 11 |- Encoder Ch1                  
               -| 12 D9           A0 12 |- Encoder Ch2                 
               -| 13 D10        AREF 13 |-X                       
               -| 14 D11         3V3 14 |-                       
               -| 15 D12     D13/SCK 15 |-                      
                +-----------------------+                       
                 Ardiono Nano                                                         
*/


// Auxiliary defines
#define IO_ACTIVE_HIGH (0)
#define IO_ACTIVE_LOW  (1)

#define IO_ON  (1)
#define IO_OFF (0)
#define IO_LEVEL(on, level) ((on) ^ (level))


// IO port defines
#define IO_TEMP_SENSOR (2)
#define IO_BOARD_LED   (13)

#define IO_RELAY_1 (3)
#define IO_RELAY_2 (4)
#define IO_RELAY_3 (5)
#define IO_RELAY_4 (6)
#define IO_RELAY_5 (7)
#define IO_RELAY_6 (8)

#define IO_ROTARY_BTN (A2)
#define IO_ROTARY_CH1 (A1)
#define IO_ROTARY_CH2 (A0)


#endif 
