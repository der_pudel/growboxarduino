
#ifndef _PUMP_HPP
#define _PUMP_HPP

void Pump_SecondTick(void);
void Pump_Run(bool run);
void Pump_StateMachine(void);
void Pump_SetParam(uint32_t at, uint32_t awt, uint32_t pt, uint32_t ct);

#endif