#ifndef _RELAY_HPP
#define _RELAY_HPP

#include "io_define.hpp"


enum RelayNr {
    RELAY_LAMP = 0,
    RELAY_FAN_LAMP,
    RELAY_AERATOR, 
    RELAY_PUMP,
    RELAY_FAN_COOLING,
    RELAY_FAN_HEATING,
};

#define SEC_BARRIER (2)  // small addional time that relay stars off (assuming that cate machine should swithc relay off before time elapsed)

void Relay_Init(void);
void Relay_halfSecTick(void);

void Relay_On(enum RelayNr nr, uint32_t time);
void Relay_Off(enum RelayNr nr);
void Relay_AllOff(void);

#endif