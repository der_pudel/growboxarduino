
#ifndef _LAMP_HPP
#define _LAMP_HPP

enum TOD { // Time of day
    TOD_DAY,
    TOD_NIGHT,
};

void Lamp_SecondTick(void);
void Lamp_Run(bool run);
void Lamp_StateMachine(void);

void Lamp_SetOnOffTime(uint32_t onTime, uint32_t offTime);

void Lamp_SetTODcalback(void (*tod_change_callback)(enum TOD));

#endif