
#ifndef _PROFILES_HPP
#define _PROFILES_HPP



struct Profile {
    char name[11];

    int8_t targetTempDay;
    int8_t targetTempNight;

    uint32_t dayTime;
    uint32_t nightTime;

    uint32_t aerationTime;
    uint32_t pumpTime;
    uint32_t cycleTime;

};

void Profile_Scroll(int8_t inc_dec);

const char * Profile_GetName(void);

int8_t Profile_GetTargetTempDay(void);
int8_t Profile_GetTargetTempNight(void);

uint32_t Profile_GetDayTime(void);
uint32_t Profile_GetNightTime(void);

uint32_t Profile_GetAerationTime(void);
uint32_t Profile_GetPumpTime(void);
uint32_t Profile_GetPumpCycleTime(void);

#endif