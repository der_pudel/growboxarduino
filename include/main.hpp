#ifndef _MAIN_HPP
#define _MAIN_HPP

#include <Arduino.h>

enum ErrType {
    ERR_TEMP_SENSOR = 1,
    ERR_OVERHEAT = 2,
    ERR_UNDEFINED = 9,
};

void ErrHandler(enum ErrType type);


void WDT_Init();
void WDT_Reset();



#endif