#ifndef _TIMER_HPP
#define _TIMER_HPP

#define DAYS_TO_SEC(x)  ( ((uint32_t)(x))*86400ul)    
#define HOURS_TO_SEC(x) ( ((uint32_t)(x))*3600ul)
#define MINS_TO_SEC(x)  ( ((uint32_t)(x))*60ul)    

#define FOREVER (0xFFFFFFFF)

void Timer_Init(void);



#endif 