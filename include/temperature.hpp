#ifndef _TEMPERATURE_HPP
#define _TEMPERATURE_HPP

void TempSensor_Init(void);

int8_t TempControl_GetTemp(void);
void TempControl_SecondTick(void);
void TempControl_StateMachine(void);


void TempControl_Run(bool run);
void TempControl_SetTarget(int8_t temp);


#endif