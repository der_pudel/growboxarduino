#ifndef _INTERFACE_HPP
#define _INTERFACE_HPP

void Display_Init(void);
void Display_DrawText(void);

void Display_DrawScr(const char *pchHeader, const char *pchMain);
void Display_ShowTemperature(uint8_t temp, const char * profile);

// Rotary functions

enum KeyEvent {
    KEY_NONE,
    KEY_PRESS,
    KEY_LONGPRESS,
    KEY_TURN_CCW,
    KEY_TURN_CW,
};


void Rotary_Init(void);

void Rotary_Scan(void);
enum KeyEvent Rotary_GetEvent(void);
void Rotary_ClearEvent(void);


#endif