#include <Arduino.h>
#include "relay.hpp"
#include "timer.hpp"


struct Relay {
    uint32_t onTime;
    const uint8_t pin;
    const uint8_t level;
};


struct Relay psRelays[] = {
   /* RELAY_LAMP */         {0, IO_RELAY_1,  IO_ACTIVE_LOW },
   /* RELAY_FAN_LAMP */     {0, IO_RELAY_2,  IO_ACTIVE_LOW },
   /* RELAY_AERATOR */      {0, IO_RELAY_3,  IO_ACTIVE_LOW },
   /* RELAY_PUMP */         {0, IO_RELAY_4,  IO_ACTIVE_LOW }, 
   /* RELAY_FAN_COOLING */  {0, IO_RELAY_5,  IO_ACTIVE_LOW },
   /* RELAY_FAN_HEATING */  {0, IO_RELAY_6,  IO_ACTIVE_LOW },
};

#define SIZE_RELAY (sizeof(psRelays)/sizeof(psRelays[0]))



void Relay_Init(void) 
{
    noInterrupts();
    for (uint8_t i = 0; i < SIZE_RELAY; i++) {
        struct Relay *r = &psRelays[i];

        r->onTime = 0;

        digitalWrite(r->pin, IO_LEVEL(IO_OFF, r->level));
        pinMode(r->pin, OUTPUT);
        
    }
    interrupts();
}


void Relay_halfSecTick(void) 
{
    for (uint8_t i = 0; i < SIZE_RELAY; i++) {
        struct Relay *r = &psRelays[i];
        if (r->onTime != FOREVER) {
            if (r->onTime && (--(r->onTime) == 0)) {
                Relay_Off((RelayNr)i);
            } 
        }
    }
}

void Relay_On(enum RelayNr nr, uint32_t seconds) 
{
    if (nr >= SIZE_RELAY) return;

    struct Relay *r = &psRelays[nr];

    noInterrupts();
    r->onTime = seconds * 2;
    interrupts();

    digitalWrite(r->pin, IO_LEVEL(IO_ON,r->level));
}

void Relay_Off(enum RelayNr nr) 
{
    if (nr >= SIZE_RELAY) return;

    struct Relay *r = &psRelays[nr];

    noInterrupts();
    r->onTime = 0;
    interrupts();

    digitalWrite(r->pin, IO_LEVEL(IO_OFF,r->level));
}

void Relay_AllOff(void) 
{
    for (uint8_t i = 0; i < SIZE_RELAY; i++) {
        Relay_Off((RelayNr)i);
    }
}

