

#include "main.hpp"

#include "profiles.hpp"
#include "timer.hpp"

int8_t index = 0;

const struct Profile profiles[] = {
//    
//     +-- Profile name                 
//     |             +-- Target temperature day 
//     |             |    +-- Target temperature night                 
//     |             |    |    +-- Day time                      
//     |             |    |    |                +-- Night time              
//     |             |    |    |                |                  +-- Aerator ON time                       
//     |             |    |    |                |                  |     +-- Pump ON time                        
//     |             |    |    |                |                  |     |  +-- Aeration/Pump cycle time                       
//     |             |    |    |                |                  |     |  |                       
    { "Strawberry",  30,  30, HOURS_TO_SEC(1), HOURS_TO_SEC(1),   20,   20, MINS_TO_SEC(30) },
    { "Potato",      40,  30, HOURS_TO_SEC(1), HOURS_TO_SEC(1),   20,   20, MINS_TO_SEC(30) },
    { "Boobies",     50,  30, HOURS_TO_SEC(1), HOURS_TO_SEC(1),   20,   20, MINS_TO_SEC(30) },
    { "Test",        40,  10, 10,              15,                5,    5,  25 },
};

#define PROFILE_COUNT (sizeof(profiles)/sizeof(profiles[0]))

void Profile_Scroll(int8_t inc_dec) {

    index += inc_dec;
    if (index < 0) index = PROFILE_COUNT - 1;
    if (index > (int8_t)(PROFILE_COUNT - 1)) index = 0;
}


const char * Profile_GetName(void) {
    return profiles[index].name;
}

int8_t Profile_GetTargetTempDay(void)
{
    return profiles[index].targetTempDay;
}

int8_t Profile_GetTargetTempNight(void)
{
    return profiles[index].targetTempNight;
}

uint32_t Profile_GetDayTime(void)
{
    return profiles[index].dayTime;
}

uint32_t Profile_GetNightTime(void)
{
    return profiles[index].nightTime;
}

uint32_t Profile_GetAerationTime(void)
{
    return profiles[index].aerationTime;
}

uint32_t Profile_GetPumpTime(void)
{
    return profiles[index].pumpTime;
}

uint32_t Profile_GetPumpCycleTime(void)
{
    return profiles[index].cycleTime;
}