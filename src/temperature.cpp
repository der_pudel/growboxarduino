#include <OneWire.h> 
#include <DallasTemperature.h>

#include "io_define.hpp"
#include "main.hpp"
#include "relay.hpp"

OneWire oneWire(IO_TEMP_SENSOR); 
DallasTemperature sensors(&oneWire);

void TempSensor_Init(void)
{
    sensors.begin(); 
}

static int8_t TempSensor_Read(void)
{
    sensors.requestTemperatures();

	DeviceAddress deviceAddress;
	if (!sensors.getAddress(deviceAddress, 0)) {
		return DEVICE_DISCONNECTED_C;
	}
    return sensors.getTemp(deviceAddress)/128;
}



#define HYSTERESIS      (5)   // °C
#define OVERHEAT_TEMP   (60)  // °C

#define MEAS_INTERVAL   (2)  // seconds


static bool bStateMachineExecute = 0;
static int8_t measuredTemp =0;
static int8_t target_temp = 0;

int8_t TempControl_GetTemp(void) 
{
    return measuredTemp;
}

void TempControl_SecondTick(void) 
{
    bStateMachineExecute = true; 
}

enum State {MEASURE, WAIT, ERROR, STANDBY_REQ, STANDBY};
static enum State tempControlState = STANDBY;

void TempControl_SetTarget(int8_t temp) 
{
    Serial.print("TOD_CHANGE\r\n");
    target_temp = temp;
    if (tempControlState != STANDBY && tempControlState != STANDBY_REQ)  {
        tempControlState = MEASURE;  //force masuremnt if tempoerature has ben changed
        bStateMachineExecute = true;
    }
}

void TempControl_Run(bool run) 
{
    if (run == false) {
        if (tempControlState != STANDBY) { tempControlState = STANDBY_REQ; }
    } else {
        if (tempControlState == STANDBY) { tempControlState = MEASURE; }
    }
}

void TempControl_StateMachine(void) 
{
    static uint16_t watiTime = 0;

    if (!bStateMachineExecute) return;
    bStateMachineExecute = false;





    switch (tempControlState) {
        case MEASURE:

            measuredTemp = TempSensor_Read();

            // Sensor disconnected check
            if (measuredTemp == DEVICE_DISCONNECTED_C) {
                tempControlState = ERROR;
                ErrHandler(ERR_TEMP_SENSOR);
                break;
            }

            // Overheat check
            if (measuredTemp >= OVERHEAT_TEMP) {
                tempControlState = ERROR;
                ErrHandler(ERR_OVERHEAT);
                break;
            }

            // Temp control
            if (measuredTemp >= target_temp + HYSTERESIS) {
                Relay_On(RELAY_FAN_COOLING, MEAS_INTERVAL + SEC_BARRIER);
                Relay_Off(RELAY_FAN_HEATING);
            } else 
            if (measuredTemp <= target_temp - HYSTERESIS) {
                Relay_Off(RELAY_FAN_COOLING);
                Relay_On(RELAY_FAN_HEATING, MEAS_INTERVAL + SEC_BARRIER);
            } else {
                 Relay_Off(RELAY_FAN_COOLING);
                 Relay_Off(RELAY_FAN_HEATING);
            }

            watiTime = MEAS_INTERVAL;
            tempControlState = WAIT;
            break;
        case WAIT:
            if (watiTime > 0) watiTime--;
            if (watiTime == 0) tempControlState = MEASURE;
            break;  
        case STANDBY_REQ:
            Relay_Off(RELAY_LAMP);
            Relay_Off(RELAY_FAN_COOLING);
            Relay_Off(RELAY_FAN_HEATING);
            Relay_Off(RELAY_FAN_LAMP);
            tempControlState = STANDBY;
            break;
        case STANDBY:
            // do nothing
            break;            
        case ERROR:
        default:
            tempControlState = ERROR; // 
            ErrHandler(ERR_UNDEFINED);
            break;
    }

}

