#include "main.hpp"
#include "lamp.hpp"
#include "relay.hpp"



bool bLampSM_Execute = false;
static uint32_t lampOnTime =0, lampOffTime = 0;
static void (*tod_callback)(enum TOD) = NULL;

void Lamp_SecondTick(void) 
{
    bLampSM_Execute = true; 
}

void Lamp_SetOnOffTime(uint32_t onTime, uint32_t offTime)
{
    lampOnTime  = onTime;
    lampOffTime = offTime;
}

void Lamp_SetTODcalback(void (*tod_change_callback)(enum TOD)) {
    tod_callback = tod_change_callback;
}

enum State {DAY, NIGHT, ERROR, STANDBY_REQ, STANDBY};
static enum State lampState = STANDBY;

void Lamp_Run(bool run) 
{
    if (run == false) {
        if (lampState != STANDBY) { lampState = STANDBY_REQ; }
    } else {
        if (lampState == STANDBY) { lampState = DAY; }
    }
}

void Lamp_StateMachine(void) 
{
    static uint32_t watiTime = 0;

    if (!bLampSM_Execute) return;
    bLampSM_Execute = false;

    switch (lampState) {

        case DAY:
            if (watiTime == 0) {
                watiTime = lampOnTime;
                if (watiTime != 0) {
                    Relay_On(RELAY_LAMP,     lampOnTime + SEC_BARRIER);
                    Relay_On(RELAY_FAN_LAMP, lampOnTime + SEC_BARRIER);
                    if (tod_callback != NULL) tod_callback(TOD_DAY);
                }
            }

            if (watiTime > 0) watiTime--;
            if (watiTime == 0) {
                lampState = NIGHT;  
            }
            break;
        case NIGHT:
            if (watiTime == 0) {
                Relay_Off(RELAY_LAMP);
                Relay_Off(RELAY_FAN_LAMP);   
                watiTime = lampOffTime;
                if (tod_callback != NULL) tod_callback(TOD_NIGHT);
            }

            if (watiTime > 0) watiTime--;
            if (watiTime == 0) {
                lampState = DAY;
            }
            break;
        case STANDBY_REQ:
            Relay_Off(RELAY_LAMP);
            Relay_Off(RELAY_FAN_LAMP);            
            lampState = STANDBY;
            break;
        case STANDBY:
            // do nothing
            watiTime = 0;
            break;            
        case ERROR:
        default:
            lampState = ERROR;
            ErrHandler(ERR_UNDEFINED);
            break;
    }

}


