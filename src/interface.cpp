

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include "io_define.hpp"
#include "interface.hpp"


#define SCREEN_WIDTH (128) // OLED display width, in pixels
#define SCREEN_HEIGHT (32) // OLED display height, in pixels

#define OLED_RESET     (-1) // Reset pin # (or -1 if sharing Arduino reset pin)

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);



void Display_Init(void) 
{

    // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
    if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
        for(;;); // Don't proceed, loop forever
    }

    display.clearDisplay();
    display.display(); 

}


void Display_DrawScr(const char *pchHeader, const char *pchMain)
{
    display.clearDisplay();
    display.setTextColor(SSD1306_WHITE);

    display.setTextSize(0);
    display.setCursor(0,0);
    display.println(pchHeader);

    display.setTextSize(2); 
    display.setCursor(0,16);  
    display.print(pchMain);   

    display.drawFastHLine(0,9, SCREEN_WIDTH, SSD1306_WHITE);

    display.display();
}


void Display_ShowTemperature(uint8_t temp, const char * profile) 
{
    char buff[32], buffHeader[32];
    sprintf(buffHeader, "Prof.: %s", profile);
    sprintf(buff, "Temp = %d%c", temp, 0xF7);

    Display_DrawScr(buffHeader, buff);
}



// Rotary functions


static enum KeyEvent keyEvent = KEY_NONE;

void Rotary_Init(void)
{
    pinMode(IO_ROTARY_BTN, INPUT_PULLUP);
    pinMode(IO_ROTARY_CH1, INPUT_PULLUP);
    pinMode(IO_ROTARY_CH2, INPUT_PULLUP);

    Rotary_ClearEvent();
}

#define ROTARY_DEBOUNCE  (20) // ms
#define ROTARY_LONGPRESS (2000) // ms

void Rotary_Scan(void) 
{
    static uint8_t prev_btn = 0xFF, prev_raw_ch1 = 0xFF, prev_raw_ch2 = 0xFF;
    static uint8_t debounce_btn = 0xFF, debounce_ch1 = 0xFF, debounce_ch2 = 0xFF;
    static bool btnPressed = false;
    static uint16_t btnPressCtrl = 0;

    static uint8_t ch1= 0xFF, ch2 = 0xFF;
    static uint8_t prev_ch1 = 0xFF;

    uint8_t tmp; 

    tmp = digitalRead(IO_ROTARY_BTN);
    if (prev_btn != tmp) {
        prev_btn = tmp;
        debounce_btn = ROTARY_DEBOUNCE;
    } else {
        if (debounce_btn != 0 && (--debounce_btn) == 0) {

            if (prev_btn == IO_LEVEL(IO_ON,IO_ACTIVE_LOW)) { // pressed
                btnPressed = true;
            } else {  // released
                btnPressed = false;
            }
        }
    }

    if (btnPressed) {
        if (btnPressCtrl >= ROTARY_LONGPRESS) {
            btnPressed = false;
            keyEvent = KEY_LONGPRESS;
            btnPressCtrl = 0;
        } else {
            btnPressCtrl++;
        }
    } else {
        if (btnPressCtrl > 0 ) {
            btnPressCtrl = 0;
            keyEvent = KEY_PRESS;
        }
    }

    tmp = digitalRead(IO_ROTARY_CH1);
    if (prev_raw_ch1 != tmp) {
        prev_raw_ch1 = tmp;
        debounce_ch1 = ROTARY_DEBOUNCE;
    } else {
        if (debounce_ch1 != 0 && (--debounce_ch1) == 0) {
            ch1 = prev_raw_ch1;
        }
    }

    tmp = digitalRead(IO_ROTARY_CH2);
    if (prev_raw_ch2 != tmp) {
        prev_raw_ch2 = tmp;
        debounce_ch2 = ROTARY_DEBOUNCE;
    } else {
        if (debounce_ch2 != 0 && (--debounce_ch2) == 0) {
            ch2 = prev_raw_ch2;
        }
    }


    if (btnPressed == false) {
        if (prev_ch1 != ch1 && prev_ch1 != 0xFF ) {
            
            if (ch1 == ch2) {
                keyEvent = KEY_TURN_CW;
            } else {
                keyEvent = KEY_TURN_CCW;
            }
        }
    }
    prev_ch1 = ch1 ;

}


enum KeyEvent Rotary_GetEvent(void) 
{
    noInterrupts();
    enum KeyEvent tmp = keyEvent;
    interrupts();
    Rotary_ClearEvent();

    return tmp;
}


void Rotary_ClearEvent(void)
{
    noInterrupts();
    keyEvent = KEY_NONE;
    interrupts();
}
