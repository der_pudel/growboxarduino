#include "timer.hpp"
#include "TimerOne.h"

#include "main.hpp"
#include "temperature.hpp"
#include "relay.hpp"
#include "interface.hpp"
#include "lamp.hpp"
#include "pump.hpp"


void timer1_ISR(void);

void Timer_Init(void)
{
    Timer1.initialize(1000); 
    Timer1.attachInterrupt(timer1_ISR); 
}



void timer1_ISR(void) {
    static uint16_t ctr_halfSec = 0x8000; 
    static uint16_t ctr_sec = 0x8000;

    Rotary_Scan();

    if (++ctr_halfSec >= 500) {
        ctr_halfSec = 0;
        // Half second events
        Relay_halfSecTick();
        WDT_Reset();
    }


    if (++ctr_sec >= 1000) {
        ctr_sec = 0;
        // Ssecond events
        TempControl_SecondTick();
        Pump_SecondTick();
        Lamp_SecondTick();
    }

}