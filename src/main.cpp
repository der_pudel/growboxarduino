
#include <avr/wdt.h>
#include "main.hpp"

#include "io_define.hpp"
#include "temperature.hpp"
#include "interface.hpp"
#include "relay.hpp"
#include "timer.hpp"
#include "profiles.hpp"
#include "pump.hpp"
#include "lamp.hpp"



static bool system_run = false;

void System_Run (bool run);
void HW_Test(void);

void WDT_Init();
void WDT_Reset();

void TOD_cahange_callback(enum TOD tod);

void setup() {
    // initialize digital pin LED_BUILTIN as an output.
    pinMode(IO_BOARD_LED, OUTPUT);

    // Serial.begin(115200);

    Relay_Init();
    TempSensor_Init();    
    Rotary_Init();
    Display_Init();

    Lamp_SetTODcalback(TOD_cahange_callback);

    Timer_Init();

    WDT_Init();    
    HW_Test();

    System_Run(false); // go to profile select
}



// the loop function runs over and over again forever
void loop() {

    enum KeyEvent event = Rotary_GetEvent();

    if (event == KEY_LONGPRESS) {
        System_Run(!system_run);  // Lonpress toggles system run state
    }

    // Profile selection is active when system is OFF
    if (system_run == false) {
        int8_t inc_dec = 0;
        if (event == KEY_TURN_CCW) { inc_dec  = -1; };
        if (event == KEY_TURN_CW ) { inc_dec  = +1; };

        if (inc_dec != 0 ) Profile_Scroll(inc_dec);
    }

    Lamp_StateMachine();    
    TempControl_StateMachine();
    Pump_StateMachine();
    
    
    if (system_run) {
        Display_ShowTemperature(TempControl_GetTemp() , Profile_GetName() );
    } else {
        Display_DrawScr("Select profile", Profile_GetName() );
    }


}


void ErrHandler(enum ErrType type)
{
    char buff[32];
    Relay_AllOff();
    sprintf(buff,  "HW Error %d", type), 
    Display_DrawScr("Error",buff);
    for (;;) {
        digitalWrite(IO_BOARD_LED, IO_LEVEL(IO_ON,IO_ACTIVE_HIGH));
        Display_DrawScr("Error",buff);
        delay(1000);
        digitalWrite(IO_BOARD_LED, IO_LEVEL(IO_OFF,IO_ACTIVE_HIGH));
        Display_DrawScr("Error","");
        delay(1000);
    };
}


void System_Run (bool run) {
    system_run = run;

    if (system_run == true) {  // Starting
        TempControl_SetTarget(Profile_GetTargetTempDay());
        Lamp_SetOnOffTime(Profile_GetDayTime(), Profile_GetNightTime());
        Pump_SetParam(Profile_GetAerationTime(), 2, Profile_GetPumpTime(), Profile_GetPumpCycleTime());
    }

    TempControl_Run(system_run);
    Lamp_Run(system_run);
    Pump_Run(system_run);
}


void TOD_cahange_callback(enum TOD tod) 
{
    int8_t temp = -1;
    if (tod == TOD_DAY) {
        temp = Profile_GetTargetTempDay();
    } else if (tod == TOD_NIGHT) {
        temp = Profile_GetTargetTempNight();
    }

    if (temp > 0 ) {
        TempControl_SetTarget(temp);
    }
}

uint8_t HW_TestRequest(const char * str1, const char * str2, uint16_t delayMs) 
{
    enum KeyEvent key;
    uint8_t result = 0;

    delay(delayMs); // small delay for showing prev message
    Display_DrawScr(str1, str2);

    Rotary_ClearEvent();
    do {
        key = Rotary_GetEvent();
        if (key == KEY_PRESS)     { result = 0; break; } // next
        if (key == KEY_LONGPRESS) { result = 1; break; }// skip
    } while(1);

    Relay_AllOff();
    return result;
}

void HW_TestProcess(void) {

    const uint8_t testDuration   = 10; // s
    const uint16_t delayDuration = 3000; // ms


    const char * strTEST = "Test";
    const char * strOK   = "OK?";

    if (HW_TestRequest(strTEST, "Press Btn", 0) != 0) return;

    Relay_On(RELAY_LAMP, testDuration);
    Relay_On(RELAY_FAN_LAMP, testDuration);
    Display_DrawScr(strTEST, "Lamp+Fan");
    if (HW_TestRequest(strTEST, strOK, delayDuration) != 0) return;

    Relay_On(RELAY_AERATOR, testDuration);
    Display_DrawScr(strTEST, "Aerator");
    if (HW_TestRequest(strTEST, strOK, delayDuration) != 0) return;

    
    Relay_On(RELAY_PUMP, testDuration);
    Display_DrawScr(strTEST, "Pump");
    if (HW_TestRequest(strTEST, strOK, delayDuration) != 0) return;

    
    Relay_On(RELAY_FAN_HEATING, testDuration);
    Display_DrawScr(strTEST, "Heat Fan");
    if (HW_TestRequest(strTEST, strOK, delayDuration) != 0) return;

    
    Relay_On(RELAY_FAN_COOLING, testDuration);
    Display_DrawScr(strTEST, "Cool Fan");
    if (HW_TestRequest(strTEST, strOK, delayDuration) != 0) return;
    
}

void HW_Test(void) {

    Relay_AllOff();
    HW_TestProcess();
    Relay_AllOff();
}



void WDT_Init() 
{

    wdt_enable(WDTO_8S);
}
void WDT_Reset() 
{
    wdt_reset();
}