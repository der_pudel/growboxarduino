

#include "main.hpp"

#include "pump.hpp"
#include "relay.hpp"

bool bPumpSM_Execute = false;

uint32_t aerationTime = 0;
uint32_t aerationWaitTime = 0;
uint32_t pumpTime = 0;
uint32_t pumpWaitTime = 0;


void Pump_SecondTick(void) 
{
    bPumpSM_Execute = true; 
}

void Pump_SetParam(uint32_t at, uint32_t awt, uint32_t pt, uint32_t ct)
{
    aerationTime = at;
    aerationWaitTime = awt;
    pumpTime = pt;

    uint32_t temp = aerationTime + aerationWaitTime + pumpTime;

    if (ct > temp) {
        pumpWaitTime = ct - temp;
    } else {
        pumpWaitTime = 0;
    }
}

enum State {AERATE, AERATE_WAIT, PUMP, PUMP_WAIT, ERROR, STANDBY_REQ, STANDBY};
static enum State pumpState = STANDBY;

void Pump_Run(bool run) 
{
    if (run == false) {
        if (pumpState != STANDBY) { pumpState = STANDBY_REQ; }
    } else {
        if (pumpState == STANDBY) { pumpState = AERATE; }
    }
}

void Pump_StateMachine(void) 
{
    static uint32_t watiTime = 0;

    if (!bPumpSM_Execute) return;
    bPumpSM_Execute = false;

    switch (pumpState) {

        case AERATE:
            if (watiTime == 0 ) {
                watiTime = aerationTime;
                if (watiTime != 0) {
                    Relay_On(RELAY_AERATOR, aerationTime + SEC_BARRIER);
                    Relay_Off(RELAY_PUMP);
                }
                
            }

            if (watiTime > 0) watiTime--;
            if (watiTime == 0) {
                pumpState = AERATE_WAIT;
            }

            break;
        case AERATE_WAIT:
            if (watiTime == 0 ) {
                Relay_Off(RELAY_AERATOR);
                Relay_Off(RELAY_PUMP);
                watiTime = aerationWaitTime;
            }

            if (watiTime > 0) watiTime--;
            if (watiTime == 0) {
                pumpState = PUMP;
            }
            break;
        case PUMP:
            if (watiTime == 0 ) {
                watiTime = pumpTime;
                if (watiTime != 0) {
                    Relay_Off(RELAY_AERATOR);
                    Relay_On(RELAY_PUMP, pumpTime + SEC_BARRIER);
                }
            }

            if (watiTime > 0) watiTime--;
            if (watiTime == 0) {
                pumpState = PUMP_WAIT;
            }
            break;
        case PUMP_WAIT:
            if (watiTime == 0 ) {
                Relay_Off(RELAY_AERATOR);
                Relay_Off(RELAY_PUMP);
                watiTime = pumpWaitTime;
                
            }
            if (watiTime > 0) watiTime--;
            if (watiTime == 0) {
                pumpState = AERATE;
            }
            break;
        case STANDBY_REQ:
            Relay_Off(RELAY_AERATOR);
            Relay_Off(RELAY_PUMP);
            pumpState = STANDBY;
            break;
        case STANDBY:
            // do nothing
            watiTime = 0;
            break;            
        case ERROR:
        default:
            pumpState = ERROR;
            ErrHandler(ERR_UNDEFINED);
            break;
    }

}
